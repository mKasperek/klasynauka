import random

ksiazki = [("13 pięter", "Filip Springer"),
           ("Angole", "Ewa Winnicka"),
           ("Astrid Lindgren. Opowieść o życiu i twórczości", "Margareta Strömstedt"),
           ("Awantury na tle powszechnego ciążenia", "Tomasz Lem"),
           ("Bajka o Rašce i inne reportaże sportowe", "Ota Pavel"),
           ("Chłopczyce z Kabulu. Za kulisami buntu obyczajowego w Afganistanie", "Jenny Nordberg"),
           ("Czyje jest nasze życie? Olga Drenda", "Bartłomiej Dobroczyński"),
           ("Detroit. Sekcja zwłok Ameryki", "Charlie LeDuff"),
           ("Głód", "Martín Caparrós"),
           ("Hajstry. Krajobraz bocznych dróg", "Adam Robiński"),
           ("Jak rozmawiać o książkach, których się nie czytało", "Pierre Bayard"),
           ("J jak jastrząb", "Helen Macdonald"),
           ("Łódź 370", "Annah Björk, Mattias Beijmo"),
           ("Matka młodej matki", "Justyna Dąbrowska"),
           ("Mężczyźni objaśniają mi świat", "Rebecca Solnit"),
           ("Nie ma się czego bać", "Justyna Dąbrowska"),
           ("Obwód głowy", "Włodzimierz Nowak"),
           ("Ostatnie dziecko lasu", "Richard Louv"),
           ("Polska odwraca oczy", "Justyna Kopińska"),
           ("Powrócę jako piorun", "Maciej Jarkowiec"),
           ("Simona. Opowieść o niezwyczajnym życiu Simony Kossak", "Anna Kamińska"),
           ("Szlaki. Opowieści o wędrówkach", "Robert Macfarlane"),
           ("Wykluczeni", "Artur Domosławski")]


class Ksiazka:
    def __init__(self, tytul, autor):
        self.tytul = tytul
        self.autor = autor
        self.ISBN = random.randint(1000000000000, 9999999999999)


class Biblioteka:
    test = False

    def __init__(self, name, stan=[]):
        self.stan = stan
        self.name = name

    def dodajKsiazke(self, ksiazka):
        if ksiazka in self.stan:
            print("byla ta ksiazka juz")
            return
        print(f"{self.name} dodaje:")
        self.ladnyPrint(ksiazka)
        print()
        self.stan.append(ksiazka)

    def oddaj_ksiazke(self, ksiazka):
        if ksiazka in self.stan:
            print(f"{self.name} przekazuje:")
            self.ladnyPrint(ksiazka)
            return self.stan.pop(self.stan.index(ksiazka))
        else:
            print("Nie mam takiej ksiazki")

    def oddaj_ksiazke_wg_tytulu(self, tytul):
        for i, ksiazka in enumerate(self.stan):
            if self.test:  # logi testowe
                print(i, ksiazka.tytul, tytul)
            if tytul in ksiazka.tytul:
                print(f"{self.name} przekazuje:")
                self.ladnyPrint(ksiazka)
                return self.stan.pop(i)

    def ladnyPrint(self, ksiazka):
        print(ksiazka.tytul, "-" * (70 - len(ksiazka.tytul)), ksiazka.autor)

    def pokajakieMaKsiazki(self):
        if not len(self.stan):
            print("nic nie czytam")
            return
        print()
        print(f"{self.name} oto twoje Ksiazki:")
        for ksiazka in self.stan:
            self.ladnyPrint(ksiazka)
        print()

    def jakieMaKsiazki(self):
        if not len(self.stan):
            return
        for ksiazka in self.stan:
            yield ksiazka.tytul, ksiazka.autor
