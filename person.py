import ksiazka


class Osoba:
    wiek = 0
    test = False

    def __init__(self, imie, nazwisko):
        self.imie = imie
        self.nazwisko = nazwisko
        self.biblioteka = ksiazka.Biblioteka(f"{imie} {nazwisko}")

    def ustalWiek(self, w):
        self.wiek = w

    def czyMogeKupicAlkochol(self):
        return True if self.wiek >= 18 else False