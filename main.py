import ksiazka, random
from person import Osoba

if __name__ == "__main__":
    # # Osoba.test = True
    # listaKsiazek = list(Ksiazka.Ksiazka(tytul, autor) for tytul, autor in random.sample(Ksiazka.ksiazki,10))
    biblioteka_publiczna = ksiazka.Biblioteka("bibliteka publiczna", list(ksiazka.Ksiazka(tytul, autor) for tytul, autor in random.sample(ksiazka.ksiazki, 10)))
    Marek = Osoba("Marek", "Kasperek")
    Ola = Osoba("Ola","Potwora")
    Marek.wiek = 28
    Ola.ustalWiek(17)
    print(Ola.czyMogeKupicAlkochol())
    print(Marek.czyMogeKupicAlkochol())
    index_ksiazki = random.randrange(len(biblioteka_publiczna.stan))
    index_ksiazki2 = random.randrange(len(biblioteka_publiczna.stan))
    Marek.biblioteka.dodajKsiazke(biblioteka_publiczna.oddaj_ksiazke(biblioteka_publiczna.stan[index_ksiazki]))
    Marek.biblioteka.dodajKsiazke(biblioteka_publiczna.oddaj_ksiazke(biblioteka_publiczna.stan[index_ksiazki]))
    Marek.biblioteka.pokajakieMaKsiazki()
    biblioteka_publiczna.dodajKsiazke(Marek.biblioteka.oddaj_ksiazke_wg_tytulu(random.choice(list(Marek.biblioteka.jakieMaKsiazki()))[0]))
    Marek.biblioteka.pokajakieMaKsiazki()
